package skynin.io;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.IOException;

import org.junit.Test;

import ua.skynin.io.ParserInput;
import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class ParserTest {

	@Test
	public void testExample() {
		/*
		 * 6 3
			101101
			111111
			111111
			3
			1 4
			2 6
			3 1
		 */
		String strInput = "6 3" + "\n" +
				"101101"  + "\n" +
				"111111" + "\n" +
				"111111" + "\n" +
				"3" + "\n" +
				"1 4" + "\n" +
				"2 6" + "\n" +
				"3 1";
		
		ParserInput parseInput = new ParserInput(
				new BufferedReader(
						new CharArrayReader(strInput.toCharArray())
					)
				);

		try {
			parseInput.parse();
		} catch (IOException e) {
			e.printStackTrace();
			fail();
			return;
		}
		
		Bundle<Line> wall = parseInput.getWall();
		Bundle<Brick> bricks = parseInput.getBricks();
		
		assertEquals(16, wall.areaSize());
		assertEquals(19, bricks.areaSize());
		
		assertEquals(2,wall.amount(6));
		assertEquals(2,wall.amount(1));
		assertEquals(1,wall.amount(2));
		
		assertEquals(4,bricks.amount(1));
		assertEquals(6,bricks.amount(2));
		assertEquals(1,bricks.amount(3));		
	}
}
