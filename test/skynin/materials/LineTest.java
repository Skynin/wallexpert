package skynin.materials;

import static org.junit.Assert.*;

import ua.skynin.materials.FactoryLine;
import ua.skynin.materials.Line;

import org.junit.Test;

public class LineTest {

	@Test
	public void test() {
		Line testLine = FactoryLine.main.getLine(3);
		
		assertEquals(3, testLine.length());
		
		Line testCopy = testLine.cutLine(FactoryLine.main.getLine(1));
		
		assertTrue(testCopy != testLine);
		
		testLine = FactoryLine.main.getLine(3);
		testCopy = FactoryLine.main.getLine(4);
		assertTrue(testLine.compareTo(testCopy) < 0);
		assertTrue(testCopy.compareTo(testLine) > 0);
		
		testCopy = testCopy.cutLine(1);
		assertTrue(testCopy.compareTo(testLine) == 0);
	}

}
