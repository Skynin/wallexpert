package skynin.materials;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	BundleTest.class,
	ExpertTest.class,
	FactoryMaterialTest.class,
	LineTest.class,
	TrialTryTest.class })
public class AllTestsSuite {
// empty
}
