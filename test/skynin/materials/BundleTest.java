package skynin.materials;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.skynin.materials.Bundle;
import ua.skynin.materials.FactoryLine;
import ua.skynin.materials.Line;

public class BundleTest {
	
	@Test
	public void testRemoveMinimal() {
		Bundle<Line> test = new Bundle<>();
		
		long[] arrRemove = new long[] {5,11,0,12, 0};
		
		for (int i = arrRemove.length; i > 0; --i) {
			test.putIn(i, arrRemove[i-1]);
		}
		test.putIn(5, 7);
		assertEquals(7, test.amount(5));
		
		arrRemove[1] = 1;
		Bundle<Line> testCopy = test.removeMinimalElements(arrRemove);
		
		assertFalse(test == testCopy);
		assertEquals(0, testCopy.amount(1));
		assertEquals(7, testCopy.amount(5));
		assertEquals(10, testCopy.amount(2));
		assertEquals(7, testCopy.amount(5));
	}
	
	@Test
	public void testOrderIterator() {
		Bundle<Line> test = new Bundle<>();
		
		test.addInFromString("0101110110011100001", '1');
		
		int currLength = 3;
		for (Line eachLine : test) {
			assertTrue(eachLine.length() == currLength);
			
			--currLength;
		}
	}	
	
	@Test
	public void testRemoveAdd() {
		Bundle<Line> test = new Bundle<>();
		
		test.addInFromString("001110110011100001", '1');
		
		Bundle<Line> test1 = test.removeAndAdd(1, 1, 2, 99);
		
		assertEquals(1, test.amount(1));
		assertFalse(test1.amount(1) == 1);

		assertEquals(1, test.amount(2));
		assertEquals(100, test1.amount(2));
		
		test1 = test1.remove(2, 100).remove(3, 2);
		
		assertTrue(test1.isEmpty());
	}

	@Test
	public void testPutFromString() {
		Bundle<Line> test = new Bundle<>();
		
		Bundle<Line> test1 = test.addInFromString("01101110110011100001", '1');
		
		assertTrue(test == test1);
		assertEquals(1, test.amount(1));
		assertEquals(2, test.amount(3));
		assertEquals(2, test.amount(2));
		
		assertEquals(3, test1.maxLine().length());
	}
	
	@Test
	public void test() {
		int lineLength = 4;
		int lineAmount = 3;
		
		Bundle<Line> test = new Bundle<>();
		
		test.putIn(lineLength, lineAmount);
		
		assertTrue(test.areaSize() == lineLength * lineAmount);
		assertTrue(test.amount(lineLength) == lineAmount);
		assertTrue(test.amount(FactoryLine.main.getLine(lineLength)) == lineAmount);
		
		Bundle<Line> testCopy = test.remove(lineLength, 1);
		
		assertTrue (testCopy != test);
		assertTrue(testCopy.areaSize() == (lineAmount - 1) * lineLength);
		assertTrue(testCopy.amount(lineLength) == lineAmount - 1);
		assertTrue(testCopy.amount(FactoryLine.main.getLine(lineLength)) == lineAmount - 1);
		
		assertFalse(testCopy.equals(test));
	}

}
