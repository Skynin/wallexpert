package skynin.materials;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;

import ua.skynin.calc.TrialContext;
import ua.skynin.calc.TrialRunner;
import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class RunnDebugTest {
	
	@Test
	public void testBig() {
		int MAX_ITER = 10_000_000;
		
		Bundle<Line> wall = new Bundle<>();
		Bundle<Brick> bricks = new Bundle<>();
		
		System.out.println("init wall");
		Random rnd = new Random();
		for (int i = 0; i < MAX_ITER; ++i) {
			wall.addIn(rnd.nextInt((int)Math.log(MAX_ITER))+Brick.MAX_LENGTH_BRICK * (1 + rnd.nextInt((int)Math.log(MAX_ITER))),
					rnd.nextInt((int)Math.log(MAX_ITER))+1);
		}
		
		System.out.println("init bricks");
		long sizeWall = wall.areaSize();
		while (sizeWall > 0) {
			int brick = rnd.nextInt(Brick.MAX_LENGTH_BRICK)+1;
			int amb = rnd.nextInt(MAX_ITER)+1;
			
			amb /= (Brick.MAX_LENGTH_BRICK - brick + 1) * Brick.MAX_LENGTH_BRICK;
			
			sizeWall -= brick * amb;
			bricks.addIn(brick, amb);
		}
		
		wall.setImmutable(true);
		bricks.setImmutable(true);
		
		byte debugMode = 0;
		boolean forkMode = true;
		TrialContext debugContext = new TrialContext(forkMode);
		debugContext.PRINT_DEBUG_TRIAL = debugMode;
		debugContext.PRINT_DEBUG_EXPERT = debugMode;
		
		// 1st
		boolean resultFork = testRun(debugContext, wall, bricks, true);
		
		System.out.println("USE_FORK:" + forkMode + " answer:" + resultFork);
		
		// 2st
		debugContext = new TrialContext(!forkMode);
		debugContext.PRINT_DEBUG_TRIAL = debugMode;
		debugContext.PRINT_DEBUG_EXPERT = debugMode;
		assertEquals(resultFork, testRun(debugContext, wall, bricks, false));
		
	}

	public boolean testRun(TrialContext context, Bundle<Line> wall, Bundle<Brick> bricks, boolean flFullInfo) {
		System.out.println("* * *");
		if (flFullInfo) {
			System.out.println("wall:");
			System.out.println(wall.toString());
			System.out.println(": " + wall.areaSize());
			System.out.println("bund: ");
			System.out.println(bricks.toString());
			System.out.println(": " + bricks.areaSize());
		}
		long msStartStart = System.currentTimeMillis();
		boolean result = new TrialRunner().answerIt(wall, bricks, context);
		long msStartEnd = System.currentTimeMillis();
		System.out.println(result + " (" + (msStartEnd - msStartStart));
		
		System.out.println("KeeK " + context.keeperK.printState());
		
		System.out.println("Fork/Rec " + context.debugCountFork.get() + "/" + context.debugCountRec.get());
		
		return result;
	}

}
