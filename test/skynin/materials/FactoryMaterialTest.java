package skynin.materials;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.skynin.materials.Brick;
import ua.skynin.materials.FactoryLine;
import ua.skynin.materials.Line;

public class FactoryMaterialTest {

	@Test
	public void test() {
		Line testLine = FactoryLine.main.getLine(Brick.MAX_LENGTH_BRICK);
		assertTrue (testLine instanceof Brick);
		
		Brick testBrick = FactoryLine.main.getBrick(Brick.MAX_LENGTH_BRICK);
		assertTrue (testLine == testBrick);
		
		testLine = FactoryLine.main.getLine(Brick.MAX_LENGTH_BRICK + 1);
		assertFalse (testLine instanceof Brick);
	}
	
	@Test(expected=ua.skynin.materials.MaterialException.class)
	public void testException() {
		Line testLine = FactoryLine.main.getLine(0);
		testLine.length();
	}
	@Test(expected=ua.skynin.materials.MaterialException.class)
	public void testException2() {
		Line testLine = FactoryLine.main.getLine(-1);
		testLine.length();
	}

}
