package skynin.materials;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.skynin.calc.TrialContext;
import ua.skynin.calc.TrialExpert;
import ua.skynin.calc.TrialPosition;
import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.FactoryLine;
import ua.skynin.materials.Line;

public class ExpertTest {

	@Test
	public void test() {
		Bundle<Line> wall = new Bundle<>();
		Bundle<Brick> bricks = new Bundle<>();
		TrialContext context = new TrialContext(true);
		
		int testLine = 13;
		int testBrick = 5;
		int testAmLine = 3;
		int testAmBrick = 100;
		
		wall.putIn(testLine, testAmLine);
		bricks.putIn(testBrick, testAmBrick);
		
		TrialExpert expert = new TrialExpert(wall, bricks, context);
		//context.PRINT_DEBUG_EXPERT = 1;
		
		TrialPosition posTrial = expert.paveMaximum(FactoryLine.main.getLine(13), FactoryLine.main.getBrick(5));
		
		assertNull(posTrial);
		
		// 2st
		wall = new Bundle<>();
		bricks = new Bundle<>();
		context = new TrialContext(false);
		//context.PRINT_DEBUG_EXPERT = 1;
		
		testAmBrick = 5;
		
		wall.putIn(testLine, testAmLine);
		bricks.putIn(testBrick, testAmBrick);

		expert = new TrialExpert(wall, bricks, context);
		
		posTrial = expert.paveMaximum(FactoryLine.main.getLine(13), FactoryLine.main.getBrick(5));
		
		assertNull(posTrial);
	}

}
