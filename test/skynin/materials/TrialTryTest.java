package skynin.materials;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ua.skynin.calc.TrialContext;
import ua.skynin.calc.TrialRunner;
import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class TrialTryTest {
	
	@Test
	public void testMiniStupor() {
		/*
	wall:
		l:17 - 4
		bund: 
		b:8 - 7
		b:7 - 7
		b:2 - 1
		: 71
	*/
		
		Bundle<Line> wall = new Bundle<>();
		wall.putIn(17, 4);
		wall.setImmutable(true);
		
		Bundle<Brick> bricks = new Bundle<>();
		bricks.putIn(8, 7);
		bricks.putIn(7, 3);
		bricks.putIn(2, 1);
		bricks.setImmutable(true);
		
		boolean forkMode = true;
		
		TrialContext debugContext = new TrialContext(forkMode);
//		debugContext.PRINT_DEBUG_TRIAL = 1;
//		debugContext.PRINT_DEBUG_EXPERT = 1;
//		debugContext.PRINT_DEBUG_MEM = 1;
		
		assertFalse(new TrialRunner(forkMode).answerIt(wall, bricks, debugContext));
	}
	@Test
	public void testForkRec() {
		/*
		 wall:
	l:17 - 6
	l:16 - 2
	l:9 - 6
	b:8 - 3
	: 212
	bund: 
	b:8 - 9
	b:7 - 7
	b:5 - 7
	b:4 - 5
	b:3 - 8
	b:2 - 16 
		 */
		Bundle<Line> wall = new Bundle<>();
		wall.putIn(17, 6);
		wall.putIn(16, 2);
		wall.putIn(9, 6);
		wall.putIn(8, 3);
		wall.setImmutable(true);
		
		Bundle<Brick> bricks = new Bundle<>();
		bricks.putIn(8, 9);
		bricks.putIn(7, 7);
		bricks.putIn(5, 7);
		bricks.putIn(4, 5);
		bricks.putIn(3, 8);
		bricks.putIn(2, 16);
		bricks.setImmutable(true);
		
		boolean forkMode = true;
		TrialContext debugContext = new TrialContext(forkMode);
		
		boolean resultPred = new TrialRunner().answerIt(wall, bricks, debugContext);
		
		debugContext = new TrialContext(!forkMode);
		assertTrue(resultPred == new TrialRunner().answerIt(wall, bricks, debugContext));
		assertTrue(resultPred);
	}
		
	@Test
	public void testExample() {
		
		for (int i=0; i < 2; ++i) {
			boolean forkMode = i==0;
		
		Bundle<Line> test = new Bundle<>();
		
		/*101101
		111111
		111111
		*/
		
		test.addInFromString("101101", '1');
		test.addInFromString("111111", '1');
		test.addInFromString("111111", '1');
		test.setImmutable(true);
		
		Bundle<Brick> bundB = new Bundle<>();
		
		/*
		 1 4
		2 6
		3 1
		 */
		bundB.putIn(1, 4);
		bundB.putIn(2, 6);
		bundB.putIn(3, 1);
		bundB.setImmutable(true);
		
		assertTrue(new TrialRunner(forkMode).answerIt(test, bundB));
		
		/*
		001100
		110011
		111111

		1 7
		3 1
		4 90
		 */
		test = new Bundle<>();
		test.addInFromString("001100", '1');
		test.addInFromString("110011", '1');
		test.addInFromString("111111", '1');
		test.setImmutable(true);
		
		 bundB = new Bundle<>();
		bundB.putIn(1, 7);
		bundB.putIn(3, 1);
		bundB.putIn(4, 90);
		bundB.setImmutable(true);
		
		assertFalse(new TrialRunner(forkMode).answerIt(test, bundB));
		
		}
	}

}
