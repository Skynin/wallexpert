package ua.skynin.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;
import ua.skynin.materials.MaterialException;

public class ParserInput {

	private Bundle<Line> wall = new Bundle<>();
	private Bundle<Brick> bricks = new Bundle<>();

	private BufferedReader inpReader;

	public ParserInput(BufferedReader inpReader) {
		this.inpReader = inpReader;
	}

	public Bundle<Line> getWall() {
		wall.setImmutable(true);
		return wall;
	}

	public Bundle<Brick> getBricks() {
		bricks.setImmutable(true);
		return bricks;
	}

	public void parse() throws IOException {
		String readStr = null;
		/*
		 * Example 6 3 101101 111111 111111 3 1 4 2 6 3 1
		 */

		// 0 - begin line = x - y wall
		// 1 - line wall
		// 2 - amount kind of brick
		// 3 - line length amount
		int stateRead = 0;

		int xArea = 0, yArea = 0;
		int currY = 0, amKindBricks = 0;
		while ((readStr = inpReader.readLine()) != null) {

			LOOP_PARSE: while (true) {
				switch (stateRead) {
				case 0:
					try (Scanner scanner = new Scanner(readStr)) {
						xArea = scanner.nextInt();
						yArea = scanner.nextInt();
					}

					if (xArea <= 0 || yArea <= 0)
						throw new IOException("Wrong wall: x " + xArea + " y " + yArea);
					++stateRead;
					break;
				case 1:
					if (currY >= yArea) {
						currY = 0;
						++stateRead;
						continue LOOP_PARSE;
					}

					if (readStr.length() != xArea)
						throw new IOException("Wrong line length: expected length" + xArea + ", there is in fact " +
								"(" + readStr + ")");

					wall.addInFromString(readStr, '1');

					++currY;
					break;
				case 2:
					try (Scanner scanner = new Scanner(readStr)) {
						amKindBricks = scanner.nextInt();
					}
					
					if (amKindBricks <= 0)
						throw new IOException("Wrong amount kind of brick: " + readStr);

					currY = 0;
					++stateRead;
					break;
				case 3:
					if (currY >= amKindBricks)
						throw new IOException("Excessive line bricks " + readStr);
					
					try (Scanner scanner = new Scanner(readStr)) {
						int lll = scanner.nextInt();
						
						if (lll > Brick.MAX_LENGTH_BRICK)
							throw new IOException("Brick too big " + lll + ". Maximum size is: " + Brick.MAX_LENGTH_BRICK);
						
						try {
							bricks.putIn(lll, scanner.nextInt());
						} catch (MaterialException eX) {
							throw new IOException(eX.getMessage(), eX);
						}
					}
					++currY;
					break;
				default:
					throw new IOException("Unknown line");
				}
				break;
			}
		}
	}
}
