package ua.skynin.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import ua.skynin.calc.TrialRunner;
import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class WallExpert {
	
	private static boolean runTrial(Bundle<Line> wall, Bundle<Brick> bricks) {
		return new TrialRunner(true).answerIt(wall, bricks);
	}
	
	private static boolean runFromFile(String fileName) throws IOException {
		File file = new File(fileName);
		
		FileReader fileReader = new FileReader(file);
		
		ParserInput parser = null;
		try (BufferedReader buffReader = new BufferedReader(fileReader)) {
			parser = new ParserInput(buffReader);

			parser.parse();
		}
		
		return runTrial(parser.getWall(), parser.getBricks());
	}
	
	@SuppressWarnings("resource")
	private static boolean runFromInput() throws UnsupportedEncodingException, IOException {
		InputStream inp = System.in;
		
		if (inp.available() > 0) {
			ParserInput parser = new ParserInput(
					new BufferedReader(
							new InputStreamReader(inp, "UTF-8")
					)
					);
		
			parser.parse();
			
			return runTrial(parser.getWall(), parser.getBricks());
		}
		
		return false;
	}
	
	private static void printResult(boolean result) {
		System.out.println(result ? "yes" : "no");
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		boolean flagFileMode = false;
		
		try {
		for (String eachString : args) {
			flagFileMode = true;
			printResult(runFromFile(eachString));
		}
		
		if (!flagFileMode) printResult(runFromInput());
		
		} catch(Exception eX) {
			printResult(false);
			
			eX.printStackTrace();
			return;
		}
	}

}
