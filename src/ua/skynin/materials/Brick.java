package ua.skynin.materials;

public class Brick extends Line {
	
	public final static int MAX_LENGTH_BRICK = 8;
	
	Brick(int len) {
		super(len);
		preffix = "b:";
	}
}
