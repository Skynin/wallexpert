package ua.skynin.materials;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class Bundle<T extends Line> implements Iterable<T> {
	
	protected final long STAMP_AM = Long.MIN_VALUE;
	
	private Map<T, Long> dataBundle = new HashMap<>();
	protected boolean isImmutable;
	
	public Bundle() {}
	
	public Bundle(boolean immutable) { isImmutable = immutable; }
	
	public boolean isImmutable() {
		return isImmutable;
	}

	public void setImmutable(boolean isImmutable) {
		this.isImmutable = isImmutable;
	}

	public Bundle(Map<T, Long> dataBundle) {
		this.dataBundle.putAll(dataBundle);
	}
	
	/**
	 * Reset inside cache value
	 */
	protected void setModify() {
		
		if (isImmutable)
			throw new MaterialException("This bundle is immutable state!");
		
		hachCache = 0;
		mxLine = null;
		arSize = STAMP_AM;
	}
	
	/**
	 * Put (instead) amount of line
	 * ! update this Bundle
	 * @param line
	 * @param amount
	 * return this bundle
	 */
	public Bundle<T> putIn(T line, long amount) {
		setModify();
		
		if (amount > 0) dataBundle.put(line, amount);
		
		return this;
	}
	/**
	 * Put (instead) amount of line
	 * ! update this Bundle
	 * @param line - length line
	 * @param amount
	 * @return this bundle
	 */
	
	@SuppressWarnings("unchecked")
	public Bundle<T> putIn(int line, long amount) {
		return putIn((T) FactoryLine.main.getLine(line), amount);
	}
	
	public Bundle<T> addIn(T line, long amount) {
		setModify();
		
		if (amount > 0) removeAndAdd(dataBundle, null, STAMP_AM, line, amount);
		
		return this;
	}
	@SuppressWarnings("unchecked")
	public Bundle<T> addIn(int line, long amount) {
		return addIn((T) FactoryLine.main.getLine(line), amount);
	}
	
	public Bundle<T> removeIn(T line, long amount) {
		setModify();
		
		if (amount > 0) removeAndAdd(dataBundle, line, amount, null, STAMP_AM);
		
		return this;
	}
	@SuppressWarnings("unchecked")
	public Bundle<T> removeIn(int line, int amount) {
		return removeIn((T) FactoryLine.main.getLine(line), amount);
	}
	
	/**
	 * Parsing str
	 * Example:
	 * by
	 * str: 0111001100111000
	 * brickSymbol: '1'
	 * line length 2 - 1
	 * line length 3 - 2
	 * @param str
	 * @param brickSymbol
	 * @return this
	 */
	public Bundle<T> addInFromString(String str, char brickSymbol) {
		
		int szStr = str.length();
		int currLen = 0;
		for(int i = 0; i < szStr; ++i) {
			char tmpChar = str.charAt(i);
			
			if (tmpChar == brickSymbol) {
				++currLen;
				continue;
			}
			
			if (currLen <= 0) continue;
				
			addIn(currLen, 1);
				
			currLen = 0;
		}
		
		if (currLen > 0) {
			addIn(currLen, 1);
		}
	
		return this;
	}
	
	public long amount(T line) {
		Long result = dataBundle.get(line);
		
		return result == null ? 0 : result;
	}
	
	/**
	 * 
	 * @param line - length line
	 * @return amount
	 */
	@SuppressWarnings("unchecked")
	public long amount(int line) {
		return amount((T)FactoryLine.main.getLine(line));
	}
	
	private long arSize = STAMP_AM;
	public long areaSize() {
		
		if (arSize >= 0) return arSize;
		
		long result = 0;
		
		for (Map.Entry<T, Long> eachPara : dataBundle.entrySet()) {
			result += eachPara.getKey().length() * eachPara.getValue();
		}
		
		return arSize = result;
	}
	
	private T mxLine;
	public T maxLine() {
		if (mxLine != null) return mxLine;
		
		Iterator<T> descIterator = this.iterator();
		
		if (descIterator.hasNext()) {
			mxLine = descIterator.next();
			
			if (descIterator.hasNext()) {
				assert descIterator.next().compareTo(mxLine) <= 0 : "Wrong iterator order";
			}
		}
		
		return mxLine;
	}
	
	public boolean isEmpty() {
		return dataBundle.isEmpty();
	}
	
	/**
	 * Add amount of line
	 * @param line
	 * @param amount
	 * @return new Bundle
	 */
	public Bundle<T> add(T line, long amount) {
		return removeAndAdd((T) null, STAMP_AM, line, amount);
	}
	
	/**
	 * Add amount of line
	 * @param line - lenght line
	 * @param amount
	 * @return new Bundle
	 */
	@SuppressWarnings("unchecked")
	public Bundle<T> add(int line, long amount) {
		return removeAndAdd((T) null, STAMP_AM, (T) FactoryLine.main.getLine(line), amount);
	}
	
	/**
	 * Remove amount of line
	 * @param line
	 * @param amount
	 * @return new Bundle
	 * @throws MaterialException if amount is more than available
	 */
	public Bundle<T> remove(T line, long amount) {
		return removeAndAdd(line, amount, (T) null, STAMP_AM);
	}
	
	/**
	 * Remove amount of line
	 * @param line - length line
	 * @param amount
	 * @return new Bundle
	 * @throws MaterialException if amount is more than available
	 */
	@SuppressWarnings("unchecked")
	public Bundle<T> remove(int line, long amount) {
		return removeAndAdd((T) FactoryLine.main.getLine(line), amount, (T) null, STAMP_AM);
	}
	
	/**
	 * РЈРґР°Р»СЏРµС‚ Р»РёРЅРёРё Р·Р°РґР°РЅРЅС‹Рµ РјР°СЃСЃРёРІРѕРј
	 * РІ 0РѕРј СЌР»РµРјРµРЅС‚Рµ - РєРѕР»РёС‡РµСЃС‚РІРѕ 1РѕР№ Р»РёРЅРёРё
	 * РІ 1РѕРј СЌР»РµРјРµРЅС‚Рµ - РєРѕР»РёС‡РµСЃС‚РІРѕ 2РѕР№ Р»РёРЅРёРё
	 * ...
	 * @param arrMinLine
	 * @return new Bundle 
	 */
	public Bundle<T> removeMinimalElements(long ... arrMinLine) {
		Bundle<T> result = new Bundle<>(dataBundle);
		
		Map<T, Long> lData = result.dataBundle;
		
		for (int i=arrMinLine.length; i > 0; --i) {
			
			long currMinLine = arrMinLine[i-1];
			
			if (currMinLine <= 0) continue;
			
			@SuppressWarnings("unchecked")
			T line = (T) FactoryLine.main.getLine(i);
			Long amLoc = lData.get(line);
			if (amLoc != null) {
				if (amLoc <= currMinLine) {
					assert amLoc == currMinLine : "debug Error removeMinimalElements";
					lData.remove(line);
				}
				else
					lData.put(line,amLoc - currMinLine);
			}
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param lineRemove
	 * @param amountRemove
	 * @param lineAdd
	 * @param amountAdd
	 * @return new Bundle
	 * @throws MaterialException if amount is more than available
	 */
	public Bundle<T> removeAndAdd(T lineRemove, long amountRemove, T lineAdd, long amountAdd) {
		Bundle<T> result = new Bundle<>(dataBundle);
		
		removeAndAdd(result.dataBundle, lineRemove, amountRemove, lineAdd, amountAdd);
		
		return result;
	}
	
	/**
	 * 
	 * @param lineRemove - lenght line
	 * @param amountRemove
	 * @param lineAdd - lenght line
	 * @param amountAdd
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Bundle<T> removeAndAdd(int lineRemove, long amountRemove, int lineAdd, long amountAdd) {
		return removeAndAdd(
				(T) FactoryLine.main.getLine(lineRemove), amountRemove,
				(T) FactoryLine.main.getLine(lineAdd), amountAdd
				);
	}
	
	/**
	 * real operation for data
	 * @param data
	 * @param lineRemove
	 * @param amountRemove
	 * @param lineAdd
	 * @param amountAdd
	 * @return
	 */
	protected Map<T, Long> removeAndAdd(Map<T, Long> data, T lineRemove, long amountRemove, T lineAdd, long amountAdd) {
		
		if (amountRemove <= 0 && amountRemove != STAMP_AM)
			throw new MaterialException("Wrong amount: line " + lineRemove + " - c:" + amountRemove);
		
		if (amountRemove > 0) {
			Long amRemove = data.get(lineRemove);
			
			if (amRemove == null || amRemove < amountRemove) throw new MaterialException("Wrong: line " + lineRemove + " - c:" + amountRemove);
			
			long newValue = amRemove - amountRemove;
			if (newValue > 0)
				data.put(lineRemove, newValue);
			else
				data.remove(lineRemove);
		}
		
		if (amountAdd <= 0 && amountAdd != STAMP_AM)
			throw new MaterialException("Wrong amount: line " + lineAdd + " - c:" + amountAdd);
		if (amountAdd > 0) {
			Long amRemove = data.get(lineAdd);
			
			if (amRemove == null) amRemove = new Long(0);
			
			data.put(lineAdd, amountAdd + amRemove);
		}		
		
		return data;
	}

	/**
	 * Next in descending order
	 */
	@Override
	public Iterator<T> iterator() {
		return (new TreeSet<>(this.dataBundle.keySet())).descendingIterator();
	}

	protected volatile int hachCache;
	@Override
	public int hashCode() {
		if (hachCache != 0) return hachCache; 
		
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataBundle == null) ? 0 : dataBundle.hashCode());
		
		return hachCache = result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Bundle<?>))
			return false;
		Bundle<?> other = (Bundle<?>) obj;
		if (dataBundle == null) {
			if (other.dataBundle != null)
				return false;
		} else if (!dataBundle.equals(other.dataBundle))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		
		StringBuilder strBui = new StringBuilder();
		for (T eachKey : this) {
			strBui.append(eachKey).append(" - ").append(dataBundle.get(eachKey)).append('\n');
		}
		String result = strBui.toString();
		return result.length() > 1 ? result.substring(0, result.length() - 1) : ":empty " + result;
	}
}
