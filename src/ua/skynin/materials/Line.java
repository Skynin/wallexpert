package ua.skynin.materials;

public class Line implements Comparable<Line> {
	
	private int length;
	
	Line(int len) {
		length = len;
	}
	
	public int length() {
		return length;
	}
	
	/**
	 * Get rest of Line
	 * @param smaller
	 * @return rest Line or null if smaller.length() == length
	 * @throws MaterialException if smaller.length > length
	 * @see differenceLine
	 */
	public Line cutLine(Line smaller) {
		if (smaller.length > length) 
			throw new MaterialException("cutting Line is bigger than this Line");
		
		if (smaller.length() == length)
			return null;
		
		return FactoryLine.main.getLine(length() - smaller.length());
	}
	
	public Line cutLine(int smaller) {
		return cutLine(FactoryLine.main.getLine(smaller));
	}

	@Override
	public int hashCode() {
		return length;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Line))
			return false;
		Line other = (Line) obj;
		if (length != other.length)
			return false;
		return true;
	}

	protected String preffix = "l:";
	@Override
	public String toString() {
		return preffix + length();
	}

	@Override
	public int compareTo(Line o) {
		if (this == o) return 0;
		if (o == null) return 1;
		
		return length() - o.length();
	}
}
