package ua.skynin.materials;

public class MaterialException extends RuntimeException {

	private static final long serialVersionUID = 5635946827300975178L;

	public MaterialException(String message) {
		super(message);
	}
}
