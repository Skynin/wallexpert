package ua.skynin.materials;

import java.util.HashMap;
import java.util.Map;

public class FactoryLine {
	
	private Map<Integer,Line> dataLine = new HashMap<>(); 
	
	private FactoryLine() {}
	
	private static Object lock = new Object();
	
	public final static FactoryLine main = FactoryLine.getInstance();
	
	// better use FactoryLine.main
	public static FactoryLine getInstance() {
		synchronized (lock) {
			if (main != null) return main;
			
			return new FactoryLine(); 
		}
	}
	
	private static Line newLine(int length) {
		return length <= Brick.MAX_LENGTH_BRICK ? new Brick(length) : new Line(length);
	}
	
	private Line get(int length) {
		Line result = dataLine.get(length);
		if (result == null) {
			result = newLine(length);
			
			dataLine.put(length, result);
		}
		
		return result;
	}
	
	/**
	 * Get Line
	 * @param length
	 * @return Line
	 * @throws MaterialException (RuntimeException) if length <= 0 
	 */
	public Line getLine(int length) {
		
		if (length <= 0) throw new MaterialException("Error Line's length: " + Integer.toString(length));
		
		return get(length);
	}

	/**
	 * Get Brick
	 * @param length
	 * @return Brick
	 * @throws MaterialException (RuntimeException) if length <= 0 || length > Utils.MAX_LENGTH_BRICK 
	 */
	public Brick getBrick(int length) {
		
		if (length <= 0 || length > Brick.MAX_LENGTH_BRICK) throw new MaterialException("Error Brick's length: " + Integer.toString(length));
		
		return (Brick) get(length);
	}
}
