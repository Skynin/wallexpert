package ua.skynin.calc;

public enum ResultTestEnum {
	NO_FIT("Area does not fit"),
	FILL_FULL("Area fill without rest"),
	FILL_IN_PART("Area fill with rest");
	
	private String description;
	
	ResultTestEnum(String pDescription) {
		description = pDescription;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
