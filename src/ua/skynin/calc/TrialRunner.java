package ua.skynin.calc;

import java.util.Iterator;
import java.util.concurrent.ForkJoinPool;

import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class TrialRunner {
	
	private ForkJoinPool fjpool = new ForkJoinPool();
	
	private boolean forkMode;
	
	public TrialRunner() {}
	
	public TrialRunner(boolean forkMode) {
		this.forkMode = forkMode;
	}
	
	public  boolean answerIt(Bundle<Line> wall, Bundle<Brick> bricks) {
		return answerIt(wall, bricks, null);
	}
	
	int countAnswer = 1;
	public boolean answerIt(Bundle<Line> wall, Bundle<Brick> bricks, TrialContext context) {
		if (context == null) context = new TrialContext(forkMode, fjpool);
		
		if (runOnce(wall, bricks, context)) return true;
		
		Iterator<TrialPosition> iterUnknown = context.iterator(KeeperKnowledge.AboutPosition.UNKNOWN);
		while (iterUnknown.hasNext()) {
			TrialPosition tryPos = iterUnknown.next();
			
			if (runOnce(tryPos.getWall(), tryPos.getBricks(), context)) return true;
		}
		
		return false;
	}
	
	private boolean runOnce(Bundle<Line> wall, Bundle<Brick> bricks, TrialContext context) {
		TrialTry trial = new TrialTry(wall, bricks, context);
		
		if (context.USE_FORK()) {
			context.setFJpool(fjpool);
			if (!fjpool.isShutdown()) fjpool.invoke(trial);
		}
		else {
			trial.compute();
		}
		
		return context.isYesAnswer();
	}
}
