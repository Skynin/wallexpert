package ua.skynin.calc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class KeeperKnowledge {
	
	public static int INIT_Capacity = 999;
	
	public enum AboutPosition {
		UNKNOWN,
		ANALYSE,
		WRONG		
	}
	
	public static class KeepKeyValue {
		/**
		 * класс является одновременно и ключем и хранилищем признака позиции
		 * поэтому statePosition не участвует в hashCode и equals
		 */
		private TrialPosition thisPosition;
		private AboutPosition statePosition = AboutPosition.UNKNOWN;
		
		public KeepKeyValue(TrialPosition position) {
			thisPosition = position;
		}
		
		public TrialPosition getTrialPosition() {
			return thisPosition;
		}
		
		public AboutPosition getStatePosition() {
			return statePosition;
		}
		
		@Override
		public int hashCode() {
			return thisPosition.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			return thisPosition.equals(obj);
		}
	}
	
	// кеш ключей-состояний
	private Map<KeepKeyValue, KeepKeyValue> allPosition = new HashMap<>(INIT_Capacity);
	
	/**
	 * Выдает ответ, одновременно помещая в allPosition
	 * использовать вместо new TrialPosition 
	 * @param wall
	 * @param bricks
	 * @return закешированную TrialPosition
	 */
	private KeepKeyValue getKeepKey(Bundle<Line> wall, Bundle<Brick> bricks) {
		
		KeepKeyValue key = new KeepKeyValue(new TrialPosition(wall, bricks));
		
		KeepKeyValue result = allPosition.get(key);
		
		if (result == null) {
			allPosition.put(key, key);
			result = key;
		}
		
		return result;
	}
	
	/**
	 * Возвращает позицию с указанным состоянием, или null, если у позиции другое состояние
	 * @param such состояние отбора позиции
	 * @param wall
	 * @param bricks
	 * @return null or position
	 */
	public synchronized TrialPosition getSuchTrialPosition(AboutPosition such, Bundle<Line> wall, Bundle<Brick> bricks) {
		KeepKeyValue answerK = getKeepKey(wall, bricks);
		
		return such == answerK.statePosition ? answerK.getTrialPosition() : null;
	}
	public synchronized void removePosition(AboutPosition such, Bundle<Line> wall, Bundle<Brick> bricks) {
		KeepKeyValue answerK = getKeepKey(wall, bricks);
		
		KeepKeyValue valueK = allPosition.get(answerK);
		
		if (valueK != null && valueK.statePosition == such) allPosition.remove(answerK);
	}
	
	
	/**
	 * Возвращает признак состояния позиции и устанавливает ее в ANALYSE
	 * @param wall
	 * @param bricks
	 * @return true, если признак уже был ANALYSE или WRONG
	 */
	public synchronized boolean getAndSetAnalyse (Bundle<Line> wall, Bundle<Brick> bricks) {
		KeepKeyValue answer = getKeepKey(wall, bricks);
		
		boolean result = answer.statePosition != AboutPosition.UNKNOWN;
		
		if (!result)
			answer.statePosition = AboutPosition.ANALYSE;
		
		return result;
	}
	
	public synchronized void setWrong(Bundle<Line> wall, Bundle<Brick> bricks) {
		getKeepKey(wall, bricks).statePosition = AboutPosition.WRONG;
	}
	
	public synchronized Iterator<TrialPosition> iterator(AboutPosition filter) {
		List<TrialPosition> result = new LinkedList<>();
		
		for (KeepKeyValue eachKey: allPosition.keySet()) {
			if (eachKey.statePosition == filter) {
				result.add(eachKey.thisPosition);
			}
		}
		
		return result.iterator();
	}
	
	@SuppressWarnings("incomplete-switch")
	public synchronized String printState() {
		StringBuilder strBui = new StringBuilder();
		
		strBui.append("allPosition:\n");
		
		int allWrong = 0, allAnalyse = 0, allUnknown = 0;
		for (KeepKeyValue eachKey: allPosition.keySet()) {
			
			switch(eachKey.statePosition) {
			case ANALYSE:
				++allAnalyse;
				break;
			case UNKNOWN:
				++allUnknown;
				break;
			default:
				++allWrong;
			}
		}
		strBui.append(":allUnknown ").append(allUnknown).append('\n');
		strBui.append(":allAnalyse ").append(allAnalyse).append('\n');
		strBui.append(":allWrong ").append(allWrong).append('\n');
		strBui.append("total ").append(allPosition.size());
		
		return strBui.toString();
	}
}
