package ua.skynin.calc;

import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class TrialPosition {
	private Bundle<Line> wall;
	private Bundle<Brick> bricks;
	
	public TrialPosition(Bundle<Line> wall, Bundle<Brick> bricks) {
		this.wall = wall;
		this.bricks = bricks;
	}
	
	public Bundle<Line> getWall() {
		return wall;
	}
	public Bundle<Brick> getBricks() {
		return bricks;
	}
	
	private volatile int hashCodeCache = 0;
	
	@Override
	public int hashCode() {
		if (hashCodeCache != 0) return hashCodeCache;
					
		final int prime = 31;
		int result = 1;
		result = prime * result + ((wall == null) ? 0 : wall.hashCode());
		result = prime * result + ((bricks == null) ? 0 : bricks.hashCode());
		return hashCodeCache = result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TrialPosition))
			return false;
		TrialPosition other = (TrialPosition) obj;
		if (wall == null) {
			if (other.wall != null)
				return false;
		} else if (!wall.equals(other.wall))
			return false;
		if (bricks == null) {
			if (other.bricks != null)
				return false;
		} else if (!bricks.equals(other.bricks))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder strBui = new StringBuilder();
		
		strBui.append("TrialPosition:\n");
		strBui.append("wall:\n").append(wall.toString()).append('\n');
		strBui.append("bund:\n").append(bricks.toString());
		
		return strBui.toString();
	}
}