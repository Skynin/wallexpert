package ua.skynin.calc;

import static ua.skynin.calc.ResultTestEnum.FILL_FULL;
import static ua.skynin.calc.ResultTestEnum.NO_FIT;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

@SuppressWarnings("serial")
public class TrialTry extends RecursiveAction {
	
	private Bundle<Line> wall;
	private Bundle<Brick> bricks;
	
	private TrialContext context;
	private TrialExpert expert;
	
	private boolean isFork;
	
	public TrialTry(Bundle<Line> wall, Bundle<Brick> bricks, TrialContext pContext) {
		this.wall = wall;
		this.bricks = bricks;
		
		this.context = pContext == null ? new TrialContext(false) : pContext;
		
		if (this.context.USE_FORK()) subTasks = new LinkedList<>();
		
		expert = new TrialExpert(wall, bricks, this.context);
	}
	
	@Override
	protected void compute() {
		if (!expert.start()) return;
		
		if (context.PRINT_DEBUG_TRIAL > 0) printDedug("compute(", null);
		else if (context.PRINT_DEBUG_MEM > 0) {
			
			long totalMem = Runtime.getRuntime().totalMemory();
			long freeMem = Runtime.getRuntime().freeMemory();
			
			if (freeMem * 1000 / totalMem == 0) {
				printDedug("compute(", null);
				
				context.printDebug(				
				"KeeK " + context.keeperK.printState() + "\n" +			
				"Fork/Rec " + context.debugCountFork.get() + "/" + context.debugCountRec.get());
			}
		}
		
		context.incForkCount(isFork);
		
		BRICK_LOOP: for (Brick eachBrick : bricks) {
			if (!expert.brick(eachBrick)) continue;
			
			// LINE_LOOP:
				for (Line eachLine : wall) {
				if (!expert.line(eachLine)) continue;
				
				ResultTestEnum resultFit = expert.testLineBreak(eachLine, eachBrick);
				
				if (resultFit == NO_FIT) continue BRICK_LOOP;
				
				TrialPosition tryPosition = expert.paveMaximum(eachLine, eachBrick);
				if (tryPosition != null) 
					startNewTrial(tryPosition.getWall(), tryPosition.getBricks());
				
				if (resultFit == FILL_FULL) {
					continue BRICK_LOOP;
				}
			}
		}
		
		expert.finis(isFork);
		
		context.decForkCount(isFork);
	}
	
	private void startNewTrial(Bundle<Line> pWall, Bundle<Brick> pBricks) {
		
		if (context.PRINT_DEBUG_TRIAL > 0) {
			if (pWall.areaSize() == wall.areaSize() && pBricks.areaSize() == bricks.areaSize()) {
				printDedug("startNewTrial(", null);
			}
		}
		
		if (context.isForkPermit()) {
			runFork(pWall, pBricks);
		}
		else {
			runRecursive(pWall, pBricks);
		}
	}
	
	private List<RecursiveAction> subTasks;
	private void waitChildTrial(TrialTry runnTrial) {
		if (subTasks.size() > context.MAX_FORK_CHILD()) {

			//wait...
			for (RecursiveAction eachAction : subTasks) {
				eachAction.join();
			}
			subTasks.clear();
		}
		subTasks.add(runnTrial);
	}
	
	private void runFork(Bundle<Line> pWall, Bundle<Brick> pBbricks) {
		// * start new TrialOwner in new Area

		TrialTry runnTrial = new TrialTry(pWall, pBbricks, context);
		
		runnTrial.isFork = true;
		waitChildTrial(runnTrial);
		if (context.fork(runnTrial)) {
			context.debugCountFork.incrementAndGet();
		}
	}
	
	private void runRecursive(Bundle<Line> pWall, Bundle<Brick> pBricks) {
		TrialTry runnTrial = new TrialTry(pWall, pBricks, context);

		if (context.PRINT_DEBUG_TRIAL > 0) {
			context.printDebug(
			"runRecursive(\n" +
			pWall.toString() + "\n" + 
			"-\n" +
			wall.toString() + "\n" +
			"--\n" + "\n" +
			pBricks.toString() + "\n" +
			"-\n" +
			bricks.toString() + "\n" +
			"runRecursive)");
		}
		
		runnTrial.isFork = false;
		runnTrial.compute();
		
		context.debugCountRec.incrementAndGet();
	}
	
	private void printDedug(String headInfo, String footer) {
		StringBuilder strBui = new StringBuilder();
		
		strBui.append(headInfo != null ? headInfo : "").append('\n');
		
		strBui.append("wall:").append('\n');
		strBui.append(wall.toString()).append('\n');
		strBui.append(": " + wall.areaSize()).append('\n');
		strBui.append("bund: ").append('\n');
		strBui.append(bricks.toString()).append('\n');
		strBui.append(": " + bricks.areaSize()).append('\n');
		
		strBui.append(footer != null ? footer : "");
		
		context.printDebug(strBui.toString());
	}
}
