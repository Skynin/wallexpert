package ua.skynin.calc;

import java.util.Iterator;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import ua.skynin.calc.KeeperKnowledge.AboutPosition;
import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class TrialContext {
	
	private AtomicBoolean isPossible = new AtomicBoolean(false);
	public final KeeperKnowledge keeperK;
	
	public final AtomicLong debugCountRec = new AtomicLong(0);
	public final AtomicLong debugCountFork = new AtomicLong(0);
	
	private AtomicInteger forkCount = new AtomicInteger(0);
	
	private int MAX_FORK_COUNT = 4;
	private int MAX_FORK_CHILD = 2;
	
	public int MAX_FORK_COUNT() { return MAX_FORK_COUNT; }
	public int MAX_FORK_CHILD() { return MAX_FORK_CHILD; }
	
	private boolean forkMode;
	private ForkJoinPool fjpool;
	
	public byte PRINT_DEBUG_TRIAL;
	public byte PRINT_DEBUG_MEM;
	public byte	PRINT_DEBUG_EXPERT;
	
	public TrialContext(boolean forkMode) {
		this(forkMode, new KeeperKnowledge());
	}
	public TrialContext(boolean forkMode, KeeperKnowledge keeKn) {
		this(forkMode, new KeeperKnowledge(), new ForkJoinPool());
	}
	public TrialContext(boolean forkMode, ForkJoinPool fjpool) {
		this(forkMode, new KeeperKnowledge(), fjpool);
	}	
	public TrialContext(boolean forkMode, KeeperKnowledge keeKn, ForkJoinPool fjpool) {
		keeperK = keeKn;
		this.forkMode = forkMode;
		this.fjpool = fjpool;
	}
	
	public ForkJoinPool getFjpool() {
		return fjpool;
	}
	public void setFJpool(ForkJoinPool fjpool) {
		this.fjpool = fjpool;
	}
	public boolean isYesAnswer() {
		return isPossible.get();
	}
	
	public boolean USE_FORK() {
		return forkMode;
	}
	
	public boolean isForkPermit() {
		return forkMode && MAX_FORK_COUNT > getForkCount();
	}

	public void setIsPossible(boolean flag) {
		isPossible.set(flag);
	}

	public int getForkCount() {
		return forkCount.get();
	}
	
	public int incForkCount(boolean isFork) {
		assert forkCount.get() >= 0 : "Bad number fork " + forkCount.get();
		
		return isFork ? forkCount.incrementAndGet() : forkCount.get();
	}
	
	public int decForkCount(boolean isFork) {
		return isFork ? forkCount.getAndDecrement() : forkCount.get();
	}
	
	public TrialPosition getUnknownTrialPosition(Bundle<Line> wall, Bundle<Brick> bricks) {
		return keeperK.getSuchTrialPosition(AboutPosition.UNKNOWN, wall, bricks);
	}
	
	public Iterator<TrialPosition> iterator(AboutPosition filter) {
		return keeperK.iterator(filter);
	}
	public boolean fork(TrialTry runnTrial) {
		if (fjpool.isShutdown()) return false;
		fjpool.execute(runnTrial);
		return true;
	}
	
	public void printDebug(String debugInfo) {
		System.out.println(debugInfo);
	}
}