package ua.skynin.calc;

import ua.skynin.materials.Brick;
import ua.skynin.materials.Bundle;
import ua.skynin.materials.Line;

public class TrialExpert {
	
	private Bundle<Line> wall;
	private Bundle<Brick> bricks;
	private TrialContext context;
	
	public TrialExpert(Bundle<Line> wall, Bundle<Brick> bricks, TrialContext context) {
		this.wall = wall;
		this.bricks = bricks;
		this.context = context;
	}
	
	private boolean isFinisWork() {
		return context.isYesAnswer();
	}
	
	/**
	 * Проверяет 
	 * @return true если разрешается старт анализа
	 */
	public boolean start() {
		assert wall.areaSize() >= 0 && bricks.areaSize() >= 0 : "Bad size area";
		
		if (success(wall, bricks)) return false;
		
		if (isImpossible(wall, bricks)) {
			context.keeperK.setWrong(wall, bricks);
			return false;
		}
		
		if (context.keeperK.getAndSetAnalyse(wall, bricks)) return false;
		
		// проверка на чей-то успех
		return !isFinisWork();
	}
	
	/**
	 * 
	 * @return true if impossible pave 
	 */
	protected boolean isImpossible(Bundle<Line> pWall, Bundle<Brick> pBricks) {
		
		if (pBricks.areaSize() == 0) return true;
		if (pWall.areaSize() > pBricks.areaSize()) return true;
		
		long amLine1 = pWall.amount(1);
		long amBricks1 = pBricks.amount(1);
		
		boolean result = amLine1 > amBricks1 ? true : false;
		
		if (!result) {
			long amBricks2 = pBricks.amount(2);
			long amLine2 = pWall.amount(2);
			
			if (amLine2 * 2 + amLine1 > amBricks2 * 2 + amBricks1) {
				return true;
			}
			
			// ищем самые короткие линии не покрываемые самыми короткими кирпичами  
			int minLengthLine = Integer.MAX_VALUE;
			long amLine = 0;
			int minLengthBrick = -1;
			for (int length= 1; length <= Brick.MAX_LENGTH_BRICK; ++length) {
				
				long ll = pWall.amount(length);
				if (ll > 0 && minLengthLine > length) {
					minLengthLine = length;
					amLine = ll; 
				}
				
				ll = pBricks.amount(length);
				if (ll > 0 && minLengthBrick <= 0) minLengthBrick = length;
				
				// самый короткий кирпич - длиннее
				if (minLengthBrick > minLengthLine) return true;
				
				// не хватит самых коротких коротких кирпичей на покрытие
				if (minLengthBrick == minLengthLine) {
					if (ll < amLine) return true;
				}
				
				if (minLengthLine != Integer.MAX_VALUE && minLengthBrick > 0)
					break;
			}
		}
		
		return result;
	}
	
	protected boolean success(Bundle<Line> pWall, Bundle<Brick> pBricks) {
		long amBricks1 = pBricks.amount(1);
		
		if (pWall.areaSize() == 0 ||
				amBricks1 >= pWall.areaSize()
				) {
			context.setIsPossible(true);
			
			if (context.PRINT_DEBUG_EXPERT > 0) {
				context.printDebug(
				"success\n"+
				"wall\n" + pWall.toString() + "\n" +
				"bricks\n" + pBricks.toString());
			}
			return true;
		}
		
		return false;
	}

	public void finis(boolean isFork) {
		// empty, for the new heuristics
	}

	public boolean brick(Brick brick) {
		// empty, for the new heuristics
		return !isFinisWork();
	}

	public boolean line(Line line) {
		// empty, for the new heuristics
		return !isFinisWork();
	}

	public ResultTestEnum testLineBreak(Line line, Brick brick) {
		int result = line.compareTo(brick);
		
		return result == 0 ? ResultTestEnum.FILL_FULL : 
			(result > 0 ? ResultTestEnum.FILL_IN_PART : ResultTestEnum.NO_FIT);
	}
	
	/**
	 * Pave the maximum
	 * @param line
	 * @param brick
	 * @return null or new wall and brikcs
	 */
	public TrialPosition paveMaximum(Line line, Brick brick) {
		int lineLenght = line.length();
		int brickLenght = brick.length();
		
		// высчитываем сколько нужно кирпичей для одной линии
		int amNeedBricksOneLine = lineLenght / brickLenght;
		
		// высчитываем остатки линии
		int cutLine = lineLenght - brickLenght * amNeedBricksOneLine;
		
		long allLines = wall.amount(line);
		long allBricks = bricks.amount(brick);
		
		// можем замостить
		long amCanPaveLine = allBricks / amNeedBricksOneLine;
		
		// сколько нужно замостить
		long amPaveLine = allLines;
		if (allLines > amCanPaveLine) {
			amPaveLine = amCanPaveLine;
		}
		// сколько понадобится
		long amPaveBricks = amPaveLine * amNeedBricksOneLine;
		
		if (amCanPaveLine == 0) {
			// ну хотя бы одну частично
			amPaveLine = 1;
			amPaveBricks = allBricks;
			cutLine = (int)(lineLenght - brickLenght * amPaveBricks);
		}
		
		if (context.PRINT_DEBUG_EXPERT > 0) {
			printDedug("paveMaximum(",
			"line " + line.toString() +
			" brick " + brick.toString() +
			" lineLenght " + lineLenght +
			" amPaveLine " + amPaveLine +
			" cutLine " + cutLine +
			" brickLenght " + brickLenght +
			" amPaveBricks " + amPaveBricks);
		}
		
		if (amPaveLine == 0 || amPaveBricks == 0) return null;

		// - - -
		Bundle<Line> resultWall = null;
		Bundle<Brick> resultBricks = null;
		if (cutLine > 0) {
			resultWall = wall.removeAndAdd(lineLenght, amPaveLine, cutLine, amPaveLine);
			resultBricks = bricks.remove(brickLenght, amPaveBricks);
		}
		else {
			resultWall = wall.remove(lineLenght, amPaveLine);
			resultBricks = bricks.remove(brickLenght, amPaveBricks);
		}
		
		if (context.PRINT_DEBUG_EXPERT > 0) {
			context.printDebug(			
			"paveMaximum) =\n"+
			"w=\n" + resultWall.toString() + "\n" +
			"b=\n" + resultBricks.toString());
		}
		
		if (isImpossible(resultWall, resultBricks)) {
			if (context.PRINT_DEBUG_EXPERT > 0)
				context.printDebug("itImpossible");
			
			return null;
		}
		TrialPosition compress = compression(resultWall, resultBricks);
		
		return compress == null ? context.getUnknownTrialPosition(resultWall, resultBricks)
				: compress;
	}
	
	/**
	 * Убирает блоки которые можно сразу закрыть
	 * @param pWall
	 * @param pBricks
	 * @return null or new compressed bundles
	 */
	protected TrialPosition compression(Bundle<Line> pWall, Bundle<Brick> pBricks) {
		TrialPosition result = null;
		
		long[] arrBrick = new long[Brick.MAX_LENGTH_BRICK];
		boolean flagPoss = false;
		
		for (int i=1; i<=Brick.MAX_LENGTH_BRICK; ++i) {
			long amLine = pWall.amount(i);
			long amBrick = pBricks.amount(i);
			
			if (amLine > 0 && amBrick > 0) {
				flagPoss = true;
				
				arrBrick[i-1] = amBrick > amLine ? amLine : amBrick;
			}
		}
		
		if (flagPoss) {
			if (context.PRINT_DEBUG_EXPERT > 0)
				context.printDebug("compression(\nwall:\n" + pWall.toString() + "\nbund:\n" + pBricks.toString());
			
			result = context.getUnknownTrialPosition(
					pWall.removeMinimalElements(arrBrick),
					pBricks.removeMinimalElements(arrBrick)
					);
			
			if (context.PRINT_DEBUG_EXPERT > 0)
				context.printDebug("compression)\n" + (result != null ? result.toString() : "null"));				
		}
			
		return result;
	}
	
	private void printDedug(String headInfo, String footer) {
		StringBuilder strBui = new StringBuilder();
		
		strBui.append(headInfo != null ? headInfo : "").append('\n');
		
		strBui.append("wall:").append('\n');
		strBui.append(wall.toString()).append('\n');
		strBui.append(": " + wall.areaSize()).append('\n');
		strBui.append("bund: ").append('\n');
		strBui.append(bricks.toString()).append('\n');
		strBui.append(": " + bricks.areaSize()).append('\n');
		
		strBui.append(footer != null ? footer : "");
		
		context.printDebug(strBui.toString());
	}
}
