WallExpert.jar
Программа выполняет проверку возможности заполнения стены данным набором фигур
Требует установленной Java 1.7 (JRE 1.7)

Информация читается из файла или из потока.
Можно указываьть несколько файлов
Примеры работы:

> java -jar WallExpert.jar exam1.txt exam2.txt
yes
no

> java -jar WallExpert.jar exam1.txt
yes

> java -jar WallExpert.jar < exam2.txt
no

Если произошла ошибка во время выполнения, то сообщение выводится в поток ошибок (STDERR)
Если не было переназначено, то после ответа 
no

(Для MS Windows)
Чтобы перенаправить сообщение об ошибке NUL, используйте следующую команду:   
java -jar WallExpert.jar file.xxx 2> nul
Кроме того, можно перенаправить выходные данные в одном месте и ошибок в другой.
java -jar WallExpert.jar file.xxx > output.msg 2> output.err
